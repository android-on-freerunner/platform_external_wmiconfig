LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

#LOCAL_ARM_MODE := arm

LOCAL_SRC_FILES:= wmiconfig.c

LOCAL_C_INCLUDES += $(LOCAL_PATH)
LOCAL_C_INCLUDES += $(TOPDIR)kernel/drivers/ar6000/include
LOCAL_C_INCLUDES += $(TOPDIR)kernel/drivers/ar6000
LOCAL_C_INCLUDES += $(TOPDIR)kernel/drivers/ar6000/ar6000

LOCAL_MODULE:= wmiconfig

LOCAL_CFLAGS += -DUSER_KEYS
LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -g

include $(BUILD_EXECUTABLE)
